import numpy as np
import codecs
import tensorflow as tf
import random

def create_model():
    #function provides a model for further her education , all of the input parameters
    model_input = tf.placeholder(tf.float32, [None, 1000])
    model_output = tf.placeholder(tf.float32, [None, 1])

    weights0 = tf.Variable(tf.random_normal([1000, 300], stddev=0.2))
    bias0 = tf.Variable(tf.random_normal([1], stddev=0.2))
    weights1 = tf.Variable(tf.random_normal([300, 1], stddev=0.2))
    bias1 = tf.Variable(tf.random_normal([1], stddev=0.2))

    h = tf.sigmoid(tf.matmul(model_input, weights0) + bias0)
    y = tf.sigmoid(tf.matmul(h, weights1) + bias1)

    loss = tf.reduce_mean(tf.square(y - model_output))
    optimizer = tf.train.AdagradOptimizer(0.2)
    train = optimizer.minimize(loss)
    data1=[]
    data1.append(model_input)
    data1.append(model_output)
    data1.append(y)
    data1.append(loss)
    data1.append(train)
    return data1

def learning(N, n, sample_size, indata, outdata):
    data=create_model()
    model_input = data[0]
    model_output = data[1]
    y = data[2]
    loss = data[3]
    train = data[4]
    init = tf.initialize_all_variables()
    sess = tf.Session()
    sess.run(init)
    iteration=0
    while iteration<N:
        for i in range(n):
            input=[]
            output=[]
            while len(input)!=sample_size:
                #the creation of vectors may take place here
                k=random.randint(0, len(indata)-1)
                input.append(indata[k])
                output.append([outdata[k]])
            _, q, y = sess.run([train, loss, y ], feed_dict={model_input: input, model_output: output})
            print "loss is "+str(q)
        iteration=iteration+1

