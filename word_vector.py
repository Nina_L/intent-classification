import re
import numpy as np
import codecs
import math

def is_number(line):
  result = re.match("[\\d]{3,6}(\.[\d]0){0,1}",line)
  if result is None:
    return False
  else:
   return (len(result.group(0))==len(line))
 
def is_number2(line):
  result = re.match("[\\d]{1,10}(\.[\d]0){0,1}",line)
  if result is None:
    return False
  else:
   return (len(result.group(0))==len(line))
 
def readfeature_dict(filename):
  f = open(filename,"r")
  p = f.readlines()
  f.close()
  dct = {}
  for px in p:
    dct[px.strip()] = px.strip()
  return dct


def check_symbol (symb):
   symtype="o"
   if symb in latUp:
     symtype = "L"
   if symb in latDown:
     symtype = "l"
   if symb in ruUp:
     symtype = "R"
   if symb in ruDown:
     symtype = "r"
   if symb in digits:
     symtype = "D"
   return symtype

def genWordShape (line):
 wshape = ""
 psym = ""
 pindex = 0
 i = 0
 for i in range(0,len(line)):
   sym = (line[i])
   symtyp = check_symbol(sym)
   if psym != symtyp:
    wshape = wshape + symtyp
   psym = symtyp
 return ("##" + wshape)

def normalize_vector(v):
    norm=np.linalg.norm(v)
    if norm==0: 
       return v
    return v/norm

projections=[]
base_dictionary={}
word_indexes=[]
words=[]

def load_projections(filename,filename1):
  #reading vectors from text file
  f = open(filename,"r")
  lines = f.readlines()
  f.close()
  for l in lines:
    l1 = l.split()
    vect = np.zeros(len(l1))
    for i in range(0,len(vect)):
      vect[i] = float(l1[i])
    projections.append(vect)
  
  #reading words to dictionary
  f = codecs.open(filename1,"r",'utf-8')
  p = f.readlines()
  
  for i in range(0,len(p)):
    #print(p[i].strip())
    base_dictionary[p[i].strip()] = i
    word_indexes.append(p[i].strip())
    
    
def has_projection (word1):
  if (is_number2(word1)):
    word = "#number" 
  else: 
   word = word1.lower()
   
  return (word in base_dictionary)
  
def get_word_index(word1):
  if is_number2(word1):
    word = "#number"
  else:
    word = word1.lower()
  if word in base_dictionary:
    return(base_dictionary[word])
  else:
   #shape =  (genWordShape(word)).lower()
   #if shape in base_dictionary:
   #  return (base_dictionary[shape])
   #else:
     return (base_dictionary["unk"])

def project_word(word):
  index = get_word_index(word)
  return projections[index]
   
def project_text_mod(text):
  spl = text.split()
  vector = np.zeros(len(projections[0]))
  cnt = 0.0
  for wrd in spl:
    if wrd in base_dictionary:
      vect = project_word(wrd)
      vector = vector + vect
      cnt = cnt + 1.0
  if cnt == 0 :
    count = 1 
  else:
    count = cnt
  return normalize_vector(vector)#vector / count)
  
def project_text_stack(text):
  spl = text.split()
  beg = np.zeros(0)
  for wrd in spl:
    beg = np.append(beg,project_word(wrd))
  return beg
  
  
def dist(v1,v2):
  return math.sqrt(sum(map(lambda x,y: (x-y)*(x-y),v1,v2)))  


def dist2(v1,v2):
    S1=math.sqrt(sum(map(lambda x: x * x , v1)))
    S2=math.sqrt(sum(map(lambda x: x * x , v2)))
    S3=sum(map(lambda x,y: x*y, v1,v2))
    rasst=S3/(S1*S2)
    return rasst


def find_closest_words(v1):
  lst = []
  for word in base_dictionary:
    wvec = project_word(word)
    dis = dist(v1,wvec)
    lst.append((dis,word))
  lst = sorted(lst)
  return lst[0:7]

def find_closest_words(v1, fun_dist):
  lst = []
  for word in base_dictionary:
    wvec = project_word(word)
    dis = fun_dist(v1,wvec)
    lst.append((dis,word))
  lst = sorted(lst)
  if fun_dist==dist2:
      lst=sorted(lst, reverse=True)
  return lst[0:7]

def superfluous_word(list_words, fun_dist):
    N=len(list_words)
    rasst=[0]*N
    for i in range(0,N-1):
        for j in range(i+1, N):
            rasst[i]=rasst[i]+fun_dist(list_words[i],list_words[j])
            rasst[j] = rasst[j] + fun_dist(list_words[i], list_words[j])
    print "distances array:"
    print rasst
    max1=0
    index=0
    for i in range(N):
        if rasst[i]>max1:
           max1=rasst[i]
           index=i
    #print "superfluous word is "
    #list_words[index]=[]
    #list_words.remove(max1)
    #list_words=filter(None, list_words)

    return index


load_projections("vectors.txt", "words.txt")