import numpy as np
import codecs
from word_vector import project_word
import tensorflow as tf
import random

infile=codecs.open("data.txt", "r", "utf-8")

category=[]
subcategory=[]
questions=[]
qt=0
zero=[0]*100

for line in infile:
    components=line.split(":")
    category.append(components[0])
    components2=components[1].split(" ")
    subcategory.append(components2[0])
    str1=""
    for i in range(1, len(components2)-1,1):
        str1=str1+components2[i]+" "
    str1=str1+"?"
    questions.append(str1)
    qt=qt+1


vectors=[]
max_len=36
for i in range(qt):
    components=questions[i].split(" ")
    for j in range(len(components)):
        if components[j]=="?":
            components[j]=""
    components=filter(None, components)
    vect = []
    for j in range(len(components)):
        #print components[j]
        tmp=project_word(components[j])
        vect.append(tmp)
        #print tmp
        #print "Vector of word is ready"
        #print len(tmp)
    if len(vect)<36:
        while(len(vect)<36):
            vect.append(zero)
    #print len(vect)
    vectors.append(vect)
#print len(vectors)
desc_cat=[]
enty_cat=[]
abbr_cat=[]
hum_cat=[]
num_cat=[]
loc_cat=[]
output=[]
for i in range(len(category)):
    if category[i]=='DESC':
        output.append([1,0,0,0,0,0])
        #print "i found DESC"
        desc_cat.append(subcategory[i])
    if category[i]=='ENTY':
        output.append([0,1,0,0,0,0])
        enty_cat.append(subcategory[i])
        #print "i found ENTY"
    if category[i] == 'ABBR':
        abbr_cat.append(subcategory[i])
        output.append([0,0,1,0,0,0])
    if category[i] == 'HUM':
        hum_cat.append(subcategory[i])
        output.append([0,0,0,1,0,0])
    if category[i] == 'NUM':
        num_cat.append(subcategory[i])
        output.append([0,0,0,0,1,0])
    if category[i] == 'LOC':
        loc_cat.append(subcategory[i])
        output.append([0,0,0,0,0,1])

desc_cat=set(desc_cat)
enty_cat=set(enty_cat)
abbr_cat=set(abbr_cat)
hum_cat=set(hum_cat)
num_cat=set(num_cat)
loc_cat=set(loc_cat)
desc_cat=list(desc_cat)
enty_cat=list(enty_cat)
abbr_cat=list(abbr_cat)
hum_cat=list(hum_cat)
num_cat=list(num_cat)
loc_cat=list(loc_cat)

all_classes={'DESC':desc_cat, 'ENTY': enty_cat, 'ABBR': abbr_cat, 'HUM': hum_cat, 'NUM': num_cat, 'LOC': loc_cat}

#print all_classes


output_end=[]
for i in range(len(output)):
    if category[i]=='DESC':
        if subcategory[i]==u'reason':
            output_end.append([1,0,0,0])
        if subcategory[i]==u'manner':
            output_end.append([0,1,0,0])
        if subcategory[i]==u'def':
            output_end.append([0,0,1,0])
        if subcategory[i]==u'desc':
            output_end.append([0,0,0,1])
    if category[i]=='ENTY':
        if subcategory[i] == u'color':
            output_end.append([1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])
        if subcategory[i] == u'plant':
            output_end.append([0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])
        if subcategory[i] == u'currency':
            output_end.append([0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])
        if subcategory[i] == u'sport':
            output_end.append([0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])
        if subcategory[i] == u'techmeth':
            output_end.append([0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])
        if subcategory[i] == u'dismed':
            output_end.append([0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])
        if subcategory[i] == u'religion':
            output_end.append([0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])
        if subcategory[i] == u'termeq':
            output_end.append([0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])
        if subcategory[i] == u'other':
            output_end.append([0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])
        if subcategory[i] == u'animal':
            output_end.append([0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])
        if subcategory[i] == u'veh':
            output_end.append([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])
        if subcategory[i] == u'body':
            output_end.append([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])
        if subcategory[i] == u'product':
            output_end.append([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0])
        if subcategory[i] == u'food':
            output_end.append([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0])
        if subcategory[i] == u'symbol':
            output_end.append([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0])
        if subcategory[i] == u'letter':
            output_end.append([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0])
        if subcategory[i] == u'word':
            output_end.append([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0])
        if subcategory[i] == u'lang':
            output_end.append([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0])
        if subcategory[i] == u'substance':
            output_end.append([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0])
        if subcategory[i] == u'cremat':
            output_end.append([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0])
        if subcategory[i] == u'instru':
            output_end.append([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0])
        if subcategory[i] == u'event':
            output_end.append([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1])
    if category[i] == 'ABBR':
        if subcategory[i]==u'abb':
            output_end.append([1,0])
        if subcategory[i]==u'exp':
            output_end.append([0,1])
    if category[i] == 'HUM':
        if subcategory[i] == u'ind':
            output_end.append([1, 0, 0, 0])
        if subcategory[i] == u'desc':
            output_end.append([0, 1, 0, 0])
        if subcategory[i] == u'gr':
            output_end.append([0, 0, 1, 0])
        if subcategory[i] == u'title':
            output_end.append([0, 0, 0, 1])
    if category[i] == 'NUM':
        if subcategory[i] == u'count':
            output_end.append([1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])
        if subcategory[i] == u'perc':
            output_end.append([0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])
        if subcategory[i] == u'code':
            output_end.append([0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])
        if subcategory[i] == u'dist':
            output_end.append([0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0])
        if subcategory[i] == u'weight':
            output_end.append([0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0])
        if subcategory[i] == u'temp':
            output_end.append([0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0])
        if subcategory[i] == u'money':
            output_end.append([0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0])
        if subcategory[i] == u'period':
            output_end.append([0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0])
        if subcategory[i] == u'other':
            output_end.append([0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0])
        if subcategory[i] == u'volsize':
            output_end.append([0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0])
        if subcategory[i] == u'date':
            output_end.append([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0])
        if subcategory[i] == u'ord':
            output_end.append([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0])
        if subcategory[i] == u'speed':
            output_end.append([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1])
    if category[i] == 'LOC':
        if subcategory[i] == u'country':
            output_end.append([1, 0, 0, 0, 0])
        if subcategory[i] == u'state':
            output_end.append([0, 1, 0, 0, 0])
        if subcategory[i] == u'other':
            output_end.append([0, 0, 1, 0, 0])
        if subcategory[i] == u'mount':
            output_end.append([0, 0, 0, 1, 0])
        if subcategory[i] == u'city':
            output_end.append([0, 0, 0, 0, 1])

# for i in range(len(output_end)):
#     print output_end[i]

input_data_train=[]
input_data_test=[]
output_data_train=[]
output_data_test=[]

for i in range(len(vectors)):
    if i<3800:
        input_data_train.append(vectors[i])
        output_data_train.append(output[i])
    else:
        input_data_test.append(vectors[i])
        output_data_test.append(output[i])

def pell_mell(a,b):
    #mix list composed for 2 vectors
    c=zip(a,b)
    random.shuffle(c)
    new_a=[]
    new_b=[]
    for i in range(len(c)):
        new_a.append(c[i][0])
        new_b.append(c[i][1])
    return new_a, new_b

def compare_output(y1, y2):
    max=y2[0]
    ind1=0
    for i in range(len(y2)):
        if y2[i]>max:
            max=y2[i]
            ind1=i
    if y1[ind1]==1:
        return 1
    else: return 0

input_data_train, output_data_train =pell_mell(input_data_train, output_data_train)

input_data_train=np.array(input_data_train)
input_data_test=np.array(input_data_test)
output_data_train=np.array(output_data_train)
output_data_test=np.array(output_data_test)


def weight_variable(shape):
    initial = tf.random_normal(shape, stddev=0.5)
    return tf.Variable(initial)


def bias_variable(shape):
    initial = tf.random_normal(shape, stddev=0.5)
    return tf.Variable(initial)


def conv2d(x, W):
    return tf.nn.conv2d(x, W, strides=[1, 1, 1, 1], padding='VALID')


# ksize: A list of ints that has length >= 4. The size of the window for each dimension of the input tensor.
# strides: A list of ints that has length >= 4. The stride of the sliding window for each dimension of the input tensor.

def max_pool(x):
    return tf.nn.max_pool(x, ksize=[1, 36, 1, 1],
                          strides=[1, 1, 1, 1], padding='VALID')

def desc (y1, x_pool):
    input=tf.concat(1, [y1,x_pool])
    print "len"
    print tf.shape(input)
    w=weight_variable([16,4])
    b=bias_variable([1])
    return y, tf.nn.softmax(tf.matmul(input, w)+b)

def enty (y1, x_pool):
    input = tf.concat(1, [y1,x_pool])
    w=weight_variable([16,22])
    b=bias_variable([1])
    return y, tf.nn.softmax(tf.matmul(input, w)+b)

def abbr (y1, x_pool):
    input = tf.concat(1, [y1,x_pool])
    w=weight_variable([16,2])
    b=bias_variable([1])
    return y, tf.nn.softmax(tf.matmul(input, w)+b)

def hum (y1, x_pool):
    input = tf.concat(1, [y1,x_pool])
    w=weight_variable([16,4])
    b=bias_variable([1])
    return y, tf.nn.softmax(tf.matmul(input, w)+b)

def num (y1, x_pool):
    input = tf.concat(1, [y1,x_pool])
    w=weight_variable([16,13])
    b=bias_variable([1])
    return y, tf.nn.softmax(tf.matmul(input, w)+b)

def loc (y1, x_pool):
    input = tf.concat(1, [y1,x_pool])
    w=weight_variable([16,5])
    b=bias_variable([1])
    return y, tf.nn.softmax(tf.matmul(input, w)+b)

xdata = tf.placeholder(tf.float32, [None, 36, 100])
ydata = tf.placeholder(tf.float32, [None, None])
y2data = tf.placeholder(tf.float32, [None, None])

Wh = weight_variable([1, 100, 1, 10])
bh = bias_variable([1])

x_shape = (tf.reshape(xdata, [-1, 36, 100, 1]))

h_conv1 = tf.nn.sigmoid(conv2d(x_shape, Wh) + bh)
h_pool = tf.reshape((max_pool(h_conv1)), [-1, 10])

W_pool=weight_variable([10, 6])
b_pool=bias_variable([1])


y=tf.nn.softmax(tf.matmul(h_pool,W_pool)+b_pool)

ind=tf.argmax(y, 1)
zero=0
one=1
two=2
three=3
ffor=4
five=5
y, sub=tf.case([(tf.equal(ind,zero), desc(y, h_pool)), (tf.equal(ind,one), enty(y, h_pool)),(tf.equal(ind,two), abbr(y, h_pool)), (tf.equal(ind,three), hum(y, h_pool)), (tf.equal(ind,ffor), num(y, h_pool)), (tf.equal(ind,five), loc(y, h_pool))],  default=desc(y, h_pool))


loss = tf.reduce_mean(tf.square((y) - ydata))
loss2=tf.reduce_mean(tf.square(sub)-y2data)
optimizer = tf.train.GradientDescentOptimizer(0.7)
train = optimizer.minimize(loss)

def learning_loss(sess):

    batch = (input_data_train, output_data_train, output_end)
    q, y2, y_end = sess.run([loss, y, sub], feed_dict={xdata: batch[0], ydata: batch[1], y2data: batch[2]})
    print "loss on this iteration: "+str(q)
    ind = 0
    for i in range(len(y2)):
        ind = ind + compare_output(batch[1][i], y2[i])
    print "accuracy="
    print float(ind) / len(y2)
    print "---------------------------------------"


def train_model(sess):
    # init = tf.initialize_all_variables()
    # sess = tf.Session()
    # sess.run(init)
    k=0
    for i in range(10):
        batch = (input_data_train[k:k+380], output_data_train[k:k+380], output_end[k:k+380])
        _ = sess.run([train], feed_dict={xdata: batch[0], ydata: batch[1], y2data: batch[2]})
        k=k+380
    return sess


def test_model(sess):
    batch = (input_data_test, output_data_test)
    yr, xshape1, c_conv, whx, bhx = sess.run([y, x_shape, h_conv1, Wh, bh],
                                             feed_dict={xdata: batch[0], ydata: batch[1]})
    ind=0
    for i in range(len(yr)):
        ind=ind+compare_output(output_data_test[i], yr[i])
    print "___________________"
    print "accuracy on test="
    print float(ind)/len(yr)
    print "___________________"


# init = tf.initialize_all_variables()
# sess = tf.Session()
# sess.run(init)
# for i in range(2000):
#     print "iteration # "+str(i)
#     sess = train_model(sess)
#     learning_loss(sess)
#     if (i%10==0 and i!=0)or i==399:
#         test_model(sess)
